﻿using UnityEngine;

namespace BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "FloatVariable", menuName = "Tools/Variables/Float")]
    public class FloatVariable : Variable<float>
    {
        
    }
}