## Required in
- [Parameter Manager](https://gitlab.com/benlaod-assets/parameter-manager)

## How to install it ?

In your package manager, click on the '+' button then 'Add package by name...'.
Then past the following URL `fr.benjaminbrasseur.gamevariables`.
