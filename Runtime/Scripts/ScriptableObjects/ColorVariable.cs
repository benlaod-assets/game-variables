﻿using UnityEngine;

namespace BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "ColorVariable", menuName = "Tools/Variables/Color")]
    public class ColorVariable : Variable<Color>
    {

    }
}