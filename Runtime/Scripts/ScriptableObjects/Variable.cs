using UnityEngine;

namespace BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects
{
    public class Variable<T> : ScriptableObject
    {
        public T value;
    }
}

