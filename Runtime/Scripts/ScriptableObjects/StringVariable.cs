﻿using UnityEngine;

namespace BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "StringVariable", menuName = "Tools/Variables/String")]
    public class StringVariable : Variable<string>
    {
        
    }
}