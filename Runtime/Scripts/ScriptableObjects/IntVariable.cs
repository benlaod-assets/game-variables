﻿using UnityEngine;

namespace BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "IntVariable", menuName = "Tools/Variables/Int")]
    public class IntVariable : Variable<int>
    {
        
    }
}